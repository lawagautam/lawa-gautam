const jsonfile = require("jsonfile");
const randomstring = require("randomstring");
const got = require("got");

const inputURL = "http://www.nactem.ac.uk/software/acromine/dictionary.py?sf=";
const outputFile = "output3.json";

var output = {}
var numberOfDefinations = 10;
console.log("getting 10 acronyms");
output.definitions = [];
console.log("creating looping function")

const getAcronym = new Promise (async function getDefinations(result, reject) {
  for(var i=0; i<numberOfDefinations; i++) {
    var acronym = randomstring.generate(3).toUpperCase();
    await got(inputURL+acronym).then(response => {
      console.log("got data for acronym", acronym);
      console.log("add returned data to definitions array");
      output.definitions.push(response.body);
  }).catch(err => {
    console.log(err)
  })
  }
  //Passing output in response
  if(output.definitions.length == numberOfDefinations) {
    result(output);
  } else {
    reject("Error");
  }
});

getAcronym.then(
  function(value) {
    console.log("saving output file formatted with 2 space indenting");
    jsonfile.writeFile(outputFile, value, {spaces: 2}, function(err) {
      console.log("All done!");
    });
  }
);