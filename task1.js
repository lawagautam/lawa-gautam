const jsonfile = require("jsonfile");
const randomstring = require("randomstring");

const inputFile = "input2.json";
const outputFile = "output2.json";

var output = {
    emails: []
}

console.log("loading input file", inputFile);

//to reverse any string
function reverseString(name) {
    var i = name.length;
    var reversedName = '';
    
    while (i > 0) {
        reversedName += name.substring(i - 1, i);
        i--;
    }
    return reversedName;
}

jsonfile.readFile(inputFile, function(err, body) {
    var data = body;

    console.log("loaded input file content", body);
    for(i=0; i<data.names.length;i++) {
        output.emails.push(
          reverseString(data.names[i]) +
          randomstring.generate(5) + 
          "@gmail.com");
    }
    console.log(output);

    //to write output in json file
    jsonfile.writeFile(outputFile, output, {spaces: 2}, function(err) {
        console.log("All done!");
    });

})